// to create react app,
	// npx create-react-app project-name

// to run application
	// npm start

// files to be removed
	//from src folder
		// App.test.js
		// index.css
		// reportWebVitals.js
		// logo.svg
		
	// we need to delete all the importation of the said files

// the "import" statement allows us to use the code/export modules from the other files similar to how we use the "require" function in NodeJS


// React JS applies the concepts of Rendering and Mounting in order to display and create components

 // Rendering refers to the process of calling/invoking component returning set of instructions creating DOM
 // Mounting is when React JS "renders or displays" the component the initial DOM based on the instructions


// use the state hook for this component to be able to store its state
	// States are used to keep track of info related to individual components
		// SYNTAX: const [getter, setter] = useState(initialGetterValue);

// useState		: maintain local component state, accessible only internally
// useEffect	: 


//alternate solution s51
/*
	const [enrollees, setEnrollees] = useState(0);
	const [seats, setSeats] = useState(30);

	function enroll(){
		if(enrollees == 30 && seats == 0){
			alert('No More Seats!');
		} else{
			setEnrollees(enrollees + 1);
			setSeats(seats - 1);			
		}

	}

*/

// using the event.target.value will capture the value inputted by the user on our input area

	// in the dependencies in useEffect
		//1. Single dependency [dependency]
			// on the initial load of the component the side effect/funtion will be triggered and whenever changes occur on our dependency
		//2. Empty dependency []
			// the sideeffect will only run during the initial load
		//3. Multiple dependency [dependency1, dependency2]
			// the side effect will run during the initial and whenever the state of the depencies change

///////ROUTING
 // npm install react-router-dom
 // needed for the routings

// [SECTION] react-router-dom
	// for us to be able to use the module/library across all of our pages we have to contain them with BrowserRouter/Router
	// Routes - we contain all of the routes in our react-app
	// Routes - specific route wherein we will declare the url and also the component

	// we use Link if we want to go from one page to another page
	// we use NavLink if we want to change our page using our NavBar


	// The "localStorage.setItem" allows us to manipulate the browser's local storage property to store information indefinitely