
import { Card , Row , Col } from 'react-bootstrap';

export default function Highlights(){

  return(
		<Row className = "mt-5">	
			{/*First Card*/}
			<Col className = "col-md-4 col-10 mx-auto m-md-0 m-2">
				<Card className = "cardHighlight">
					<Card.Body>
						<Card.Title className = "text-center">Learn from Home</Card.Title>					    
						<Card.Text>
							Some quick example text to build on the card title and make up the bulk of the card's content.
						</Card.Text>
					</Card.Body>
				</Card>
			</Col>

			{/*Second Card*/}
			<Col className = "col-md-4 col-10 mx-auto m-md-0 m-2">
				<Card className = "cardHighlight">
					<Card.Body>
						<Card.Title className = "text-center">Study Now, Pay Later!</Card.Title>					   
						<Card.Text className = "text-justify">
							Some quick example text to build on the card title and make up the bulk of the card's content.
						</Card.Text>
					</Card.Body>
				</Card>
			</Col>
			
			{/*Third Card*/}
			<Col className = "col-md-4 col-10 mx-auto m-md-0 m-2">
				<Card className = "cardHighlight">
				<Card.Body>
					<Card.Title className = "text-center">Be Part of Our Community</Card.Title>					    
					<Card.Text className = "text-justify">
						Some quick example text to build on the card title and make up the bulk of the card's content.
					</Card.Text>
				</Card.Body>
				</Card>
			</Col>
	  </Row>
  )

}