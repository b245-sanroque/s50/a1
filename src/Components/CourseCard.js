
import { Row , Col , Card , Button } from 'react-bootstrap';
import { useState , useEffect , useContext } from 'react';
import { Link } from 'react-router-dom';

import UserContext from '../UserContext.js'

export default function CourseCard({courseProp}){

	const { _id , name , description , price } = courseProp;
	// the course in the CourseCard component is called a prop which is shorthand for property
	// the curly braces are used for props to signify that we are providing information

	// use the state hook for this component to be able to store its state
	// States are used to keep track of info related to individual components
		// SYNTAX: const [getter, setter] = useState(initialGetterValue);

	const [enrollees, setEnrollees] = useState(0);

	const {user} = useContext(UserContext);

	 // initial values of enrollees state
		// console.log(enrollees);

	 // if you want to reassign/change the value of the state
		// setEnrollees(1);
		// console.log(enrollees);

/*		
	Activity Code for S51
	function enroll(){			
		setEnrollees(enrollees + 1);
		// cannot use increment,=+ kasi marereassign si enrollees w/c is not possible
		taken()
		// console.log(enrollees);		

		if(enrollees >= 30){
			setEnrollees(enrollees);
			available(seats);
			alert('No More Seats!');
		}
	}
	const [seats, available] = useState(30);
	 // console.log(seats)
	 function taken(){
			available(seats - 1);
		}
*/
	
	//alternate sol-s51
	const [seats, setSeats] = useState(30);

	function enroll(){
		if(enrollees < 29 && seats > 1){
			setEnrollees(enrollees + 1);
			setSeats(seats - 1);	
		} else{
			alert('You enrolled for the last slot! Congratulations for making it!');	
			setEnrollees(enrollees + 1);
			setSeats(seats - 1);	
		}
	}

  // Define a useEffect hook to have the CourseCard component to perform a certain task
	// SYNTAX: useEffect(sideEffect/function, [dependencies]);

		// sideEffect/function - will run on the first load and will reload depending on the dependency array

		//
		const [isDisabled, setisDisabled] = useState(false);

		useEffect(() => {
			if(seats === 0){
				setisDisabled(true)
			}
		}, [seats]);


  return(
  	
	<Row className = "mt-5">	
	  <Col>
        <Card className = "col-10 mx-auto">
		  <Card.Body>

			<Card.Title className = "text-center">{name}</Card.Title>	

			<Card.Subtitle className = "pt-2">Description:</Card.Subtitle>
			<Card.Text>{description}</Card.Text>

			<Card.Subtitle>Price:</Card.Subtitle>
			<Card.Text>Php {price}</Card.Text>

			<Card.Subtitle>Enrollees:</Card.Subtitle>
			<Card.Text>{enrollees}</Card.Text>

			<Card.Subtitle>Available Seats:</Card.Subtitle>
			<Card.Text>{seats}</Card.Text>

			{/*conditional rendering/ternary operator*/}
			{
				user ?
				<Button as = {Link} to = {`/course/${_id}`} disabled = {isDisabled}>See more Details</Button>
				:
				<Button as = {Link} to = "/login">Login to enroll!</Button>
			}

		  </Card.Body>
		</Card>
	  </Col>
	</Row>
  )

}
