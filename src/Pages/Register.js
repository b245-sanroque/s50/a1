
import { Button , Form , Row , Col } from 'react-bootstrap';
import { Navigate , useNavigate , Link } from 'react-router-dom';

//import the hooks that are needed in our page
import { Fragment , useState , useEffect , useContext } from 'react';

import Swal from 'sweetalert2';

import UserContext from '../UserContext.js'


export default function Register(){
	// create 3 new states where we will store the value from input of the mail, password and confirmPassword
	const [firstName, setFirstName] = useState('');
	const [lastName, setLastName] = useState('');
	const [email, setEmail] = useState('');
	const [mobileNo, setMobileNo] = useState('');
	const [password, setPassword] = useState('');
	const [confirmPassword, setConfirmPassword] = useState('');

	// const [user, setUser] = useState(localStorage.getItem("email"));
	const { user , setUser } = useContext(UserContext)

	const navigate = useNavigate();

	// create another state for the button
	const [isActive, setIsActive] = useState(false);

	// // multiple dependency array
	// useEffect(() => {
	// 	console.log(email)
	// 	console.log(password)
	// 	console.log(confirmPassword)
	// }, [email,password,confirmPassword])

	useEffect(() => {
		if(
			firstName !== "" && 
			lastName !== "" && 
			email !== "" && 
			mobileNo !== "" && 
			password !== "" && 
			confirmPassword !== "" && 
			password === confirmPassword
			){
				setIsActive(true);
		} else{
			setIsActive(false);
		}
	}, [email,password,confirmPassword])

	function register(event){
		event.preventDefault();

		//fetch request from API (register)
		fetch(`${process.env.REACT_APP_API_URL}/user/register`, { //endpoint
			method: 'POST',
			headers: {
				'Content-Type' : 'application/json'
			},
			body: JSON.stringify({
				firstName: firstName,
				lastName: lastName,
				email: email,
				mobileNo: mobileNo,
				password: password,
				confirmPassword: confirmPassword
			})
		}).then(result => result.json() //convert result to json
		).then(data => {
			console.log(data);

			Swal.fire({
				title: "Registration Success!",
				icon: "success",
				text: "Thank you for registering! Check your email for a confirmation and Login to your account. After that, you can now enroll courses offered by Zuitt! Enjoy!"
			})

			navigate("/login");
		})

/*		alert("Congratulation! You are now registered to our website.")

		localStorage.setItem("email", email)
		setUser(localStorage.getItem("email"));

		setEmail('');
		setPassword('');
		setConfirmPassword('');

		navigate("/");*/
	}

	  const [isCapsLockOn, setIsCapsLockOn] = useState(false);
	  // This function is triggered on the keyup event
	    const checkCapsLock = (event) => {
	      if (event.getModifierState('CapsLock')) {
	        setIsCapsLockOn(true);
	      } else {
	        setIsCapsLockOn(false);
	      }
	    }

	 /* // ermm.. next time
	  const [isPasswordMatch, setIsPasswordMatch] = useState(true);
	    const checkPassword = (event) => {
	      if(password === confirmPassword) {
	        setIsPasswordMatch(true);
	      } else {
	        setIsPasswordMatch(false);
	      }
	    }*/

	return(
	  user ?
	  <Navigate to = "*" />
	  :
	  <Row className = "mt-5">
		<Col className = "col-md-6 col-10 mx-auto bg-light p-3">
		  <Fragment>
		    <h1 className = "text-center">Register</h1>
			<Form className = "mt-5" onSubmit = {event => register(event)}>

				<Form.Group className="mb-3" controlId="formRegisterFirstName">
					<Form.Label className = "fw-bold">First Name</Form.Label>
					<Form.Control 
						type="text" 
						placeholder="First Name" 
						value = {firstName}
						onChange = {event => setFirstName(event.target.value)}
						required
						/>
				</Form.Group>

				<Form.Group className="mb-3" controlId="formRegisterLastName">
					<Form.Label className = "fw-bold">Last Name</Form.Label>
					<Form.Control 
						type="text" 
						placeholder="Last Name" 
						value = {lastName}
						onChange = {event => setLastName(event.target.value)}
						required
						/>
				</Form.Group>

				<Form.Group className="mb-3" controlId="formRegisterMobileNo">
					<Form.Label className = "fw-bold">Mobile Number</Form.Label>
					<Form.Control 
						type="text" 
						placeholder="(+63) 9123456789" 
						value = {mobileNo}
						onChange = {event => setMobileNo(event.target.value)}
						required
						/>
				</Form.Group>

				<Form.Group className="mb-3" controlId="formRegisterEmail">
					<Form.Label className = "fw-bold">Email Address</Form.Label>
					<Form.Control 
						type="email" 
						placeholder="Email@mail.com" 
						value = {email}
						onChange = {event => setEmail(event.target.value)}
						required
						/>
					<Form.Text className="text-muted fw-lighter"> We'll never share your email with anyone else. </Form.Text>
				</Form.Group>

				<Form.Group className="mb-3" controlId="formRegisterPassword">
					<Form.Label className = "fw-bold">Password</Form.Label>
					<Form.Control 
						onKeyUp = { checkCapsLock }
						type="password" 
						placeholder="Password" 
						value = {password}
						onChange = {event => setPassword(event.target.value)}
						required
						/>
					<Row>
						<Form.Text className="text-muted fw-lighter">Case Sensitive</Form.Text>
						<Form.Text className="text-muted fw-lighter"> Password must contain at least 1 special character. haha</Form.Text>
					</Row>	
				</Form.Group>

				<Form.Group className="mb-3" controlId="formRegisterConfirmPassword">
					<Form.Label className = "fw-bold">Confirm Password</Form.Label>
					<Form.Control 
						onKeyUp = { checkCapsLock }
						type="password" 
						placeholder="Confirm Password" 
						value = {confirmPassword}
						onChange = {event => setConfirmPassword(event.target.value)}

						required
						/>

					{isCapsLockOn && (
					  <p className = "text-danger">Caps Lock is ON</p>
					)}

					{/*errr. pano magtwo events? haha
					{!isPasswordMatch && (
					  <p className = "text-danger">Password does not MATCH</p>
					)}*/}


				</Form.Group>

				{/*In this code block we do conditional rendering depending on the state of our inActive*/}
				{    
					isActive ? 
					<Button variant="primary" type="submit">
						Submit
					</Button>
					:
					<Button variant="danger" type="submit" disabled>
						Submit
					</Button>
				}
				<Form.Text> Already have an account? Login <Link to = "/login">here</Link>.</Form.Text>



			</Form>
		  </Fragment>
		</Col>
	  </Row> 
	)

}