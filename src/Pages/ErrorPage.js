
import { Fragment } from 'react';
import { Row , Col } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function ErrorPage() {
  
  return (
  	  <Fragment>
  	  	<Row className = "text-center mt-5">
  	  	  <Col className = "mx-auto p-3">
			<h1>The Page you are accessing is NOT FOUND!</h1>
			<p className = "mt-5">Click <Link to = "/">here</Link> to go back to Home Page.</p>
			<p>Click <Link to = "/register">here</Link> to Register an Account.</p>
			<p>If you already have an account, click <Link to = "/login">here</Link> to login.</p>
		  </Col>
		</Row>
	  </Fragment>
  )

}