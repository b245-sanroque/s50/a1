
import { Button , Form , Row , Col } from 'react-bootstrap';

import { Fragment , useState , useEffect, useContext } from 'react';
import { Navigate , useNavigate , Link } from "react-router-dom";

import Swal from 'sweetalert2';

import UserContext from '../UserContext.js';

export default function Login(){

	//Login
	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');

	//contain to var
	const navigate = useNavigate();

	// const [user, setUser] = useState(localStorage.getItem('email'));
	// remove/comment out, since nasa UserProvider na

	// allows us to consume the UserContext obj and it's properties for user validation

	const {user, setUser} = useContext(UserContext);
	// console.log(user);

	//Button
	const [isActive, setIsActive] = useState(false);

	useEffect(() => {
		if(email !== "" && password !== ""){
			setIsActive(true);
		} else{
			setIsActive(false);
		}
	}, [email,password])


	function login(event){
		event.preventDefault();

		// If you want add the email of the authenticated user in the local storage
			//SYNTAX: localStorage.setItem(propertyName, value)

		/*S55-Process fetach request to corresponding backend API*/
		// Syntax: fetch('url', {options});

		fetch(`${process.env.REACT_APP_API_URL}/user/login`, { //endpoint
			method: 'POST',
			headers: {
				'Content-Type' : 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		}).then(result => result.json() //convert result to json
		).then(data => {
			console.log(data)
			if(data === false){
				Swal.fire({
					title: "Authentication Failed",
					icon: "error",
					text: "Please try again :>"
				})
			} else{
				localStorage.setItem('token', data.auth);
				retrieveUserDetails(localStorage.getItem('token'));

				//if successfully logged in
				Swal.fire({
					title: "Authentication Success",
					icon: "success",
					text: "Welcome to Zuitt!"
				})

				navigate("/");
			}
		})

/*		localStorage.setItem("email", email)
		setUser(localStorage.getItem("email"));

		alert("Welcome! You are now logged in.")
		setEmail('');
		setPassword(''); 
		navigate("/");*/
		// useNavigate("/"); cannot use Hook inside a function

		const retrieveUserDetails = (token) => {
			// the token sent as part of the request's header info

			fetch(`${process.env.REACT_APP_API_URL}/user/details`, {
				headers: { //if no method indicated, automatic "GET"
					Authorization: `Bearer ${token}`
				}
			}).then(result => result.json()
			).then(data => {
				console.log(data);

				setUser({
					id: data._id,
					isAdmin: data.isAdmin
				})
			})
		}

	}//login


	const [isCapsLockOn, setIsCapsLockOn] = useState(false);
	// This function is triggered on the keyup event
	  const checkCapsLock = (event) => {
	    if (event.getModifierState('CapsLock')) {
	      setIsCapsLockOn(true);
	    } else {
	      setIsCapsLockOn(false);
	    }
	  }


  return(
  	user ?
	<Navigate to = "*"/>
	:
	<Row className = "mt-5">
	  <Col className = "col-md-6 col-10 mx-auto bg-light p-3">
	    <Fragment>
		  <h1 className = "text-center">LOGIN</h1>
			<Form className = "mt-5" onSubmit = {event => login(event)}>
			  <Form.Group className="mb-3" controlId="formLoginEmail">
				<Form.Label className = "fw-bold">Email Address</Form.Label>
				  <Form.Control 
					type="email" 
					placeholder="Enter Email" 
					value = {email}
					onChange = {event => setEmail(event.target.value)}
					required
				  />
			  </Form.Group>

			  <Form.Group className="mb-3" controlId="formLoginPassword">
				<Form.Label className = "fw-bold">Password</Form.Label>
				  <Form.Control
				  	onKeyUp = { checkCapsLock } 
					type="password" 
					placeholder="Enter Password" 
					value = {password}
					onChange = {event => setPassword(event.target.value)}
					required
				  />
				  <Form.Text className="text-muted fw-lighter">Case Sensitive</Form.Text>
				 
				  {isCapsLockOn && (
				    <p className = "text-danger">Caps Lock is ON</p>
				  )}
			  </Form.Group>

			  {    
				isActive ? 
				<Button variant="primary" type="submit">
					Submit
				</Button>
				:
				<Button variant="danger" type="submit" disabled>
					Submit
				</Button>
			  }

			  <Form.Text> Dont't have an account yet? Create one now! Register <Link to = "/register">here</Link>.</Form.Text>

			</Form>
		  </Fragment>
	  	</Col>
	  </Row>  
	)//return 

 }//Login