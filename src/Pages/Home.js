import {Fragment} from 'react';

import Banner from '../Components/Banner.js';
import Highlights from '../Components/Highlights.js';

export default function Home(){

	return(
		<Fragment>
			<Banner/>
			<Highlights/>
		</Fragment>
	)

}