
import { Fragment , useEffect , useState } from 'react';

// import coursesData from "../database/courses.js"; //no need for mock database "courses" 
import CourseCard from "../Components/CourseCard.js";
        
export default function Courses() {
    // console.log(coursesData); //no need for mock database "courses"

/*    const courses = coursesData.map(course => {

    	return( 
    		<CourseCard key = {course.id} courseProp = {course}/>
    	
    	)
    })*/

    // contain courses using useState
    const [courses, setCourses] = useState([]); //empty array

    useEffect(()=>{
        fetch(`${process.env.REACT_APP_API_URL}/course/allActive`)
        .then(result => result.json())
        .then(data => {
            console.log(data);
            // to change the value of our courses so we have to use the setCourses
            setCourses(data.map(course => {
                return( 
                    <CourseCard key = {course._id} courseProp = {course}/>
                )
            }))
        })
    }, [])

    return(
    	<Fragment>
            <h1 className = "text-center mt-3">COURSES</h1>
        	{courses}

    	</Fragment>
    )
}

