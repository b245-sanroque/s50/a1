import React from 'react';

// Create a Context Object
// A context object as the name states is a data type of an object that can be used to store info that can be shared to other components within the app
// The context object is a diff approach to passing info bet. components and allos easier access by avoicing the use of prop drilling.

const UserContext = React.createContext();

// The "Provider" property of createCOntext allows other components to  consume/use the context object and supply the necessary info
export const UserProvider = UserContext.Provider;

export default UserContext;