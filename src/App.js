
import './App.css';

// import {Fragment} from 'react';
// import { Container } from 'react-bootstrap';

// importing AppNavBar function from the AppNavBar.js
 import AppNavBar from './Components/AppNavBar.js';
 import CourseView from './Components/CourseView.js';

// imports from pages
 import Home from './Pages/Home.js';
 import Courses from './Pages/Courses.js';
 import Register from './Pages/Register.js';
 import Login from './Pages/Login.js';
 import Logout from './Pages/Logout.js';
 import ErrorPage from './Pages/ErrorPage.js';

//import modules from react-router-dom for the routing
 import { BrowserRouter as Router, Routes, Route} from 'react-router-dom';

import { useState , useEffect } from 'react';

// import the UserProvider
import { UserProvider } from './UserContext.js';

function App() {

  const [user, setUser] = useState(null);
  
    useEffect(()=>{
      console.log(user)
    }, [user])

  const unSetUser = () => {
    localStorage.clear();
    // setUser(localStorage.getItem("email"));
  }

  useEffect(()=>{
    fetch(`${process.env.REACT_APP_API_URL}/user/details`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    }).then(result => result.json()
    ).then(data => {
      console.log(data);
      if(localStorage.getItem('token') !== null){
          setUser({
            id: data._id,
            isAdmin: data.isAdmin
          })
      } else{
          setUser(null);
      }
        
    })
  },[]);

  // Storing info in a context obj is done by providing the info using the corresponding "Provider" and passing the info thru the prop value
  // all info/data provided to the provider component

  return (

    <UserProvider value = { { user , setUser , unSetUser } }>
      <Router>
        <AppNavBar/>
        <Routes>
            <Route path = '*' element = {<ErrorPage/>} />
            <Route path="/" element = {<Home/>} />
            <Route path = "/courses" element ={<Courses/>} />
            <Route path ="/login" element = {<Login/>}/>
            <Route path = '/register' element = {<Register/>} />
            <Route path = '/logout' element = {<Logout/>} />
            <Route path = '/course/:courseId' element = {<CourseView/>} />
        </Routes>

      </Router>
    </UserProvider>
  );
}

export default App;
